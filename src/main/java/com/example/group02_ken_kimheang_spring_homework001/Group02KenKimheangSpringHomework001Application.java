package com.example.group02_ken_kimheang_spring_homework001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;


@SpringBootApplication

public class Group02KenKimheangSpringHomework001Application {
    private static final List<Customer> customers = new ArrayList<>();

    public static void main(String[] args) {

        SpringApplication.run(Group02KenKimheangSpringHomework001Application.class, args);

    }

}
