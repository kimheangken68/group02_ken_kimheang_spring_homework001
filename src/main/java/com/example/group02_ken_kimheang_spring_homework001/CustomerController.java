package com.example.group02_ken_kimheang_spring_homework001;

import jakarta.annotation.PostConstruct;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.util.*;


@RestController
@RequestMapping("/api/v1/customers")
public class CustomerController {
    private  List<Customer> customers = new ArrayList<>();
    Map<String, Object> map = new LinkedHashMap<>();

    @PostConstruct
    void addData(){
        Customer customer1 = new Customer(1,"Smy","Female",20,"PP");
        Customer customer2 = new Customer(2,"Vid","Male",22,"KPS");
        Customer customer3 = new Customer(3,"Nathath","Male",25,"KPS");
        customers.add(customer1);
        customers.add(customer2);
        customers.add(customer3);
    }


    // Listing Customers
    @GetMapping
    public ResponseEntity<?> getUser() {
        if (!customers.isEmpty()) {
            map.clear();
            map.put("message", "You get all customer successfully!");
            map.put("data", customers);
            map.put("status", "OK");
            map.put("time",Instant.now() );
            return new ResponseEntity<>(map, HttpStatus.OK);
        } else {
            map.clear();
            map.put("status", "NOT FOUND");
            map.put("message", "Data is not found!");
            return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
        }
    }

    // Create Customer
    @PostMapping
    public ResponseEntity<?>  createProduct(@RequestBody CustomerRequest customerRequest ) {
        Customer customer1 = new Customer();
        String name = customerRequest.getName();
        String gender = customerRequest.getGender();
        String address = customerRequest.getAddress();
        Integer age = customerRequest.getAge();
        Integer id = customers.get(customers.size()-1).getId() + 1;

        Customer customer = new Customer(id,name,gender,age,address);
        customers.add(customer);

        map.clear();
        map.put("message", "This record was successfully created!");
        map.put("data", customer);
        map.put("status", "OK");
        map.put("time", Instant.now());

        return new ResponseEntity<>(map, HttpStatus.CREATED);
    }

    //Get Customer by ID
    @GetMapping("/{customId}")
    public ResponseEntity<?>   getCustomerByID(@PathVariable Integer customId) {
        for (Customer customer : customers) {
            if (customer.getId().equals(customId)) {
                map.clear();
                map.put("message","This record has found successfully!");
                map.put("data",customer);
                map.put("status","OK");
                map.put("time",Instant.now());
                return new ResponseEntity<>(map,HttpStatus.OK);
            }
        }
        map.clear();
        map.put("message","Record not found!");
        map.put("status","NOT FOUND");
        return new ResponseEntity<>(map,HttpStatus.NOT_FOUND);
    }

//    Update customer
    @PutMapping("/{customId}")
    public ResponseEntity<?>  updateCustomer(@PathVariable Integer customId, @RequestBody CustomerRequest updateCustomer) {
        for (Customer customer : customers) {
            if (customer.getId().equals(customId)) {
                customer.setName(updateCustomer.getName());
                customer.setGender(updateCustomer.getGender());
                customer.setAge(updateCustomer.getAge());
                customer.setAddress(updateCustomer.getAddress());

                map.put("message","You're update successfully!");
                map.put("data",customer);
                map.put("status","OK");
                map.put("time",Instant.now());
                return new ResponseEntity<>(map,HttpStatus.OK);
            }
        }
        map.clear();
        map.put("message","Record not found!");
        map.put("status","NOT FOUND");
        return new ResponseEntity<>(map,HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{customId}")
    public ResponseEntity<?> deleteCustomerById (@PathVariable Integer customId ){
        for(Customer customer1 : customers){
            if(customer1.getId().equals(customId)){
                customers.removeIf(customer -> customer.getId().equals(customId));
                map.clear();
                map.put("message","Congratulation your delete is successfully!");
                map.put("status","OK");
                map.put("time",Instant.now());
                return new ResponseEntity<>(map,HttpStatus.OK);
            }
        }
        map.clear();
        map.put("message","Record not found!");
        map.put("status","NOT FOUND");
        return new ResponseEntity<>(map,HttpStatus.NOT_FOUND);
    }

    @GetMapping("/search")
    public ResponseEntity<?> getCustomerByName(@RequestParam (name = "name") String name){
        for(Customer customer : customers){
            if(customer.getName().equals(name)){
                map.clear();
                map.put("message","This record has found successfully!");
                map.put("data",customer);
                map.put("status","OK");
                map.put("time",Instant.now());
                return new ResponseEntity<>(map,HttpStatus.OK);
            }
        }
        map.clear();
        map.put("message","Record not found!");
        map.put("status","NOT FOUND");
        return new ResponseEntity<>(map,HttpStatus.NOT_FOUND);
    }

}
